# Nostradamus

Playing with prediction algorithms.  

Code from Twitch lives, replays (in French) available:  

XGBoost  
- https://www.youtube.com/watch?v=IjE9NNC32zw  
- https://www.youtube.com/watch?v=HbMWNprAzHI  

Prophet  
- https://www.youtube.com/watch?v=Kr8OmCx3u50

## Setup

Clone this repository:  

    git clone https://gitlab.com/databulle/livestream/nostradamus.git  
    cd nostradamus  

Create virtual environment.  
I recommend using Python 3.8.16:  

    pyenv install 3.8.16
    pyenv virtualenv 3.8.16 nostradamus
    pyenv local nostradamus

Install requirements:  

    pip install -r requirements.txt  

All set!  

## Usage

Launch Jupyter local server:  

    jupyter notebook  

Open http://localhost:8888 in your browser.  

